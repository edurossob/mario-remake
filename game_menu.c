#include "game_menu.h"

void home_menu_draw()
{
    int font_size = al_get_font_line_height(font);

/*  al_draw_text(
            font,
            al_map_rgb_f(0,0,0),
            BUFFER_W / 2, BUFFER_H / 2,
            ALLEGRO_ALIGN_CENTER,
            "M A R I O"
        );
  */

    al_draw_bitmap(sprites.mario_logo, (BUFFER_W - 220) /2, (BUFFER_H - 96)/2, 0);

    al_draw_text(
            font,
            al_map_rgb_f(0,0,0),
            BUFFER_W / 2, BUFFER_H - 30 ,
            ALLEGRO_ALIGN_CENTER,
            "DIGITE ENTER PARA JOGAR!"
        );

    al_draw_text(
            font,
            al_map_rgb_f(0.3,0.3,0.3),
            BUFFER_W / 2, BUFFER_H - 10 ,
            ALLEGRO_ALIGN_CENTER,
            "DIGITE Q PARA SAIR"
        );
    
    al_draw_text(
            font,
            al_map_rgb_f(0.2,0.2,0.2),
            BUFFER_W - 5, 30,
            ALLEGRO_ALIGN_RIGHT,
            "Melhores Scores"
        );

    for (int i = 0; i < MAX_N_SCORES; i++)
    {
        if(best_scores[i] == 0)
            continue;
        al_draw_textf(
            font,
            al_map_rgb_f(0.2,0.2,0.2),
            BUFFER_W - 5, 30 + (i+1)*(font_size+ 2),
            ALLEGRO_ALIGN_RIGHT,
            "%d", best_scores[i]
        );
    }
    
    

}

void home_menu_fade_out()
{
    float b;

    b = 1-(frames/180.0);


    al_draw_text(
            font,
            al_map_rgb_f(b, b, b),
            BUFFER_W / 2, BUFFER_H / 2,
            ALLEGRO_ALIGN_CENTER,
            "M A R I O"
        );
}

void game_menu_draw(){
    if(!is_paused)
        return;

    al_draw_filled_rectangle( // Escurece o background
        BUFFER_W, BUFFER_H, 0, 0, 
        al_map_rgba_f(0, 0, 0, 0.3)
        );

    al_draw_filled_rectangle( // Desenha retânculo principal
        BUFFER_W/4, BUFFER_H/4, BUFFER_W-(BUFFER_W/4), BUFFER_H-(BUFFER_H/4), 
        al_map_rgba_f(0, 0, 0, 0.9)
        );


    al_draw_text(
        font,
        al_map_rgb_f(1,1,1),
        BUFFER_W / 2, (BUFFER_H / 4) + 10,
        ALLEGRO_ALIGN_CENTER,
        "P A U S E D"
    );

    al_draw_text(
        font,
        al_map_rgb_f(1,1,1),
        BUFFER_W / 4 + 5, (BUFFER_H / 4) + 25,
        ALLEGRO_ALIGN_LEFT,
        "M - muta o som"
    );

    al_draw_text(
        font,
        al_map_rgb_f(1,1,1),
        BUFFER_W / 4 + 5, (BUFFER_H / 4) + 38,
        ALLEGRO_ALIGN_LEFT,
        "SETAS - Controles"
    );

    al_draw_text(
        font,
        al_map_rgb_f(1,1,1),
        BUFFER_W / 4 + 5, (BUFFER_H / 4) + 50,
        ALLEGRO_ALIGN_LEFT,
        "H ou F1 - Pausa"
    );

    al_draw_text(
        font,
        al_map_rgb_f(1,1,1),
        BUFFER_W / 4 + 5, (BUFFER_H / 4) + 59,
        ALLEGRO_ALIGN_LEFT,
        "Q - Fecha jogo"
    );


    al_draw_text(
        font,
        al_map_rgb_f(1,1,1),
        (BUFFER_W / 2), BUFFER_H - (BUFFER_H / 4) - 36,
        ALLEGRO_ALIGN_CENTER,
        "FEITO POR"
    );

    al_draw_text(
        font,
        al_map_rgb_f(1,1,1),
        (BUFFER_W / 2), BUFFER_H - (BUFFER_H / 4) - 26,
        ALLEGRO_ALIGN_CENTER,
        "EDUARDO R. BARBOSA"
    );

    al_draw_text(
        font,
        al_map_rgb_f(1,1,1),
        (BUFFER_W / 2), BUFFER_H - (BUFFER_H / 4) - 16,
        ALLEGRO_ALIGN_CENTER,
        "GRR20190378"
    );

}

void post_menu_draw()
{
    al_draw_filled_rectangle( // Escurece o background
        BUFFER_W, BUFFER_H, 0, 0, 
        al_map_rgba_f(0, 0, 0, 0.3)
        );

    al_draw_filled_rectangle( // Desenha retânculo principal
        BUFFER_W/4, BUFFER_H/4, BUFFER_W-(BUFFER_W/4), BUFFER_H-(BUFFER_H/4), 
        al_map_rgba_f(0, 0, 0, 0.9)
        );


    al_draw_text(
        font,
        al_map_rgb_f(1,1,1),
        BUFFER_W / 2, (BUFFER_H / 4) + 10,
        ALLEGRO_ALIGN_CENTER,
        "G A M E  O V E R"
    );


    al_draw_text(
        font,
        al_map_rgb_f(1,1,1),
        (BUFFER_W / 2), BUFFER_H - (BUFFER_H / 4) - 26,
        ALLEGRO_ALIGN_CENTER,
        "ENTER para retornar"
    );
}

    

#include <stdio.h>
#include <stdlib.h>

#include "hud.h"
#include "map_handler.h"
#include "keyboard.h"
#include "audio.h"
#include "playable_character.h"
#include "game_menu.h"
#include "score_handler.h"


ALLEGRO_TIMER* timer;
ALLEGRO_EVENT_QUEUE* queue;
ALLEGRO_EVENT event;



// Funções de lógica da main
void pause_unpause()
{
    is_paused = !is_paused;
    mute_unmute_music();
}

void check_end_of_game()
{
    if(got_hit)
        pc.lives --;

    if(pc.lives == 0 || seconds_played == MAX_TIME){
        is_game_ended = true;
    }
}

void frames_update()
{
    if(is_paused)
        return;

    if(is_game_ended)
        return;

    frames++;

    if(frames < 450) // Inicia a partir de 450 frames devido ao tempo de espera por causa da musica
        return;

    if(frames % 60 == 0) // A cada 60 frames, ou 1 segundo
        seconds_played ++;
}


// Telas do jogo
void entering_game_transition_screen()
{
// -- TRANSIÇÃO --

    frames = 0;
    al_drain_audio_stream(home_menu_music);
    while(frames < 180 && !really_done) // Espera 3 segundos
    {
        disp_pre_draw();
            
        al_clear_to_color(al_map_rgb(135,206,235)); // valor RGB referente ao céu azul  

        home_menu_fade_out();

        disp_post_draw();

        frames++;
    }
}

void home_screen()
{
    bool done;
    bool redraw;

    done = false;
    frames = 0;
    game_score = 0;
    score_display = 0;
    coin = 0;
    
    redraw = 0;
    is_paused = false;
    is_game_ended = false;
    seconds_played = 0;


    pc_revive();

    audio_reinit(home_menu_music);

    while(!really_done) 
    {
        al_wait_for_event(queue, &event);

        switch (event.type)
        {
            case ALLEGRO_EVENT_TIMER:
                redraw = true;
                frames++;
            break;
            case ALLEGRO_EVENT_KEY_DOWN:
                if(event.keyboard.keycode == ALLEGRO_KEY_ENTER){
                    done = true;
                    play_select_sound();
                }

                if(event.keyboard.keycode == ALLEGRO_KEY_ESCAPE || event.keyboard.keycode == ALLEGRO_KEY_Q)
                    really_done = true;
            break;
            

            case ALLEGRO_EVENT_DISPLAY_CLOSE:
                printf("Programa finalizado manualmente\n");
                really_done = true;
                break;
        }

        if(done)
            break;

        keyboard_update(&event);

        if(redraw && al_is_event_queue_empty(queue))
        {
            disp_pre_draw();
            
            al_clear_to_color(al_map_rgb(135,206,235)); // valor RGB referente ao céu azul  

            home_menu_draw();

            redraw = false;

            disp_post_draw();
        }
    }



}

void death_transition_screen()
{
    int redraw;
    int done;

    redraw = false;
    done = false;

    frames = 0;

    al_detach_audio_stream(main_music);

    while (!really_done)
    {
        al_wait_for_event(queue, &event);
        
        switch (event.type)
        {
            case ALLEGRO_EVENT_TIMER:
                frames_update();

                redraw = true;
            break;

            case ALLEGRO_EVENT_KEY_DOWN:

                if(event.keyboard.keycode == ALLEGRO_KEY_ESCAPE || event.keyboard.keycode == ALLEGRO_KEY_Q)
                    really_done = true;
            break;

            case ALLEGRO_EVENT_DISPLAY_CLOSE:
                printf("Programa finalizado manualmente\n");
                really_done = true;
            break;
        }

        if(done)
            break;

        keyboard_update(&event);

        if(redraw && al_is_event_queue_empty(queue))
        {
            disp_pre_draw();

            al_clear_to_color(al_map_rgb(135,206,235)); // valor RGB referente ao céu azul
    
            tiles_draw();
            npc_draw();
            done = pc_death_animation_draw();
            hud_draw();


            disp_post_draw();
            redraw = false;
        }
    }
}

void game_screen()
{
    int done;
    int redraw;

    done = false;
    redraw = false;


    // -- coloca no inicio do nível

    npc_reinit();
    pc_reinit();
    map_reinit();
    audio_reinit(main_music);


    got_hit = false;

    while(!really_done)
    {
        al_wait_for_event(queue, &event);
        switch (event.type)
        {
            case ALLEGRO_EVENT_TIMER:
                frames_update();

                npc_update();

                pc_update();

                map_update();
                
                hud_update();           

                redraw = true;
                
            break;

            case ALLEGRO_EVENT_KEY_DOWN:
                if(event.keyboard.keycode == ALLEGRO_KEY_M || event.keyboard.keycode == ALLEGRO_KEY_RSHIFT){
                    is_muted = ! is_muted;
                    mute_unmute_music();
                }
                
                if(event.keyboard.keycode == ALLEGRO_KEY_F1 || event.keyboard.keycode == ALLEGRO_KEY_H)
                    pause_unpause();

                if(event.keyboard.keycode == ALLEGRO_KEY_ESCAPE || event.keyboard.keycode == ALLEGRO_KEY_Q)
                    really_done = true;
                
            break;

            case ALLEGRO_EVENT_DISPLAY_CLOSE:
                printf("Programa finalizado manualmente\n");
                done = true;
                really_done = true;
            break;
        }

        if(done || got_hit)
            break;
            
        keyboard_update(&event);

        if(redraw && al_is_event_queue_empty(queue))
        {
            disp_pre_draw();

            al_clear_to_color(al_map_rgb_f(0.41,0,0)); // valor RGB referente ao céu Vermelho
            if(frames < 435)
                al_clear_to_color(al_map_rgb(135,206,235)); // valor RGB referente ao céu azul
    
            tiles_draw();
            npc_draw();
            pc_draw();
            hud_draw();

            game_menu_draw();

            disp_post_draw();
            redraw = false;
        }

    }    

    death_transition_screen();
}

void game_over_screen()
{
    int redraw;
    int done;

    redraw = false;
    done = false;

    frames = 450; 

    while(!really_done) 
    {
        al_wait_for_event(queue, &event);


        switch (event.type)
        {
            case ALLEGRO_EVENT_TIMER:
                redraw = true;
                frames++;
            break;

            case ALLEGRO_EVENT_KEY_DOWN:
                if(event.keyboard.keycode == ALLEGRO_KEY_ENTER)
                    done = true;

                if(event.keyboard.keycode == ALLEGRO_KEY_ESCAPE || event.keyboard.keycode == ALLEGRO_KEY_Q)
                    really_done = true;
            break;
            
            case ALLEGRO_EVENT_DISPLAY_CLOSE:
                printf("Programa finalizado manualmente\n");
                really_done = true;
            break;
        }

        if(done)
            break;

        keyboard_update(&event);

        if(redraw && al_is_event_queue_empty(queue))
        {
            disp_pre_draw();

            al_clear_to_color(al_map_rgb_f(0.41,0,0)); // valor RGB referente ao céu Vermelho
    
            tiles_draw();
            npc_draw();
            pc_draw();
            hud_draw();

            post_menu_draw();

            disp_post_draw();
        }
    }
}


int main(int argc, char const *argv[])
{
    // Inicializações gerais

    keyboard_init();

    disp_init();

    audio_init();

    sprites_init();

    npc_init();

    map_init();

    score_init();

    hud_init();

    pc_init();

    timer = al_create_timer(1.0 / 60.0);
    must_init(timer, "timer");

    queue = al_create_event_queue();
    must_init(queue, "queue");

    must_init(al_init_primitives_addon(), "primitives");

    al_register_event_source(queue, al_get_keyboard_event_source());
    al_register_event_source(queue, al_get_display_event_source(disp));
    al_register_event_source(queue, al_get_timer_event_source(timer));

    keyboard_set();


    really_done = false;

    al_start_timer(timer);


    while(!really_done)
    {
        // -- HOME --

        home_screen();

        al_detach_audio_stream(home_menu_music);

        // -- Transição para o jogo 

        entering_game_transition_screen();
        
        // -- JOGO --

        frames = 0;

        while(!is_game_ended) // Inicia e reinicia o jogo, sai apenas quando o jogo acaba ou clica-se no x na janela
        {
            // -- Lógica interna do jogo (vidas)
            game_screen();

            check_end_of_game();     
              

            if(really_done)
                break;
            
            if(is_game_ended)
                break;

            frames = 220;
        }

        // -- GAME OVER SCREEN --

        game_over_screen();

    }



    // De-inicializações gerais

    sprites_deinit();
    map_deinit();
    hud_deinit();
    audio_deinit();
    disp_deinit();
    pc_deinit();
    npc_deinit();
    score_deinit();
    al_destroy_timer(timer);
    al_destroy_event_queue(queue);
    
    return 0;
}

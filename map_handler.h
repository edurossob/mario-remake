#ifndef __MAP_HANDLER__
#define __MAP_HANDLER__

    #include <allegro5/allegro.h>
    #include <string.h>

    #include "common.h"
    #include "sprites.h"
    #include "keyboard.h"
    #include "display.h"
    #include "playable_character.h"
    #include "npc.h"
    #include "audio.h"

    char MAP_NAME[32]; // Nome do mapa atual
    int MAP_W; // Width do mapa atual
    int MAP_H; // Heigth do mapa atual
    int VISION_DIFF; // Valor que mexe a câmera



    typedef enum TILE_TYPE
    {
        TILE_TYPE_GROUND = 1, // Usaremos o valor 0 para espaço vazio
        TILE_TYPE_BRICK_TOP,
        TILE_TYPE_BRICK,
        TILE_TYPE_QUESTION_ON,
        TILE_TYPE_QUESTION_OFF,
        TILE_TYPE_SQUARE,
        TILE_TYPE_N
    } TILE_TYPE;

    typedef enum TILE_PRIZE
    {
        PRIZE_NONE,
        PRIZE_COIN,
        PRIZE_LIFE,
        PRIZE_MUSHROOM,
        PRIZE_N
    } TILE_PRIZE;

    typedef struct TILE
    {
        int x;
        int y;
        TILE_TYPE type;
        bool is_broken;
        int prize;
    } TILE;


    #define TILE_W 15 // TILE é todo bloco do jogo, seja chão, question_mark ou tijolo 
    #define TILE_H 15 // TILE tem tamanho 16 (ou 0~15)

    #define CAMERA_CENTER_X  BUFFER_W/2


    FILE * map_file;

    TILE ** LEVEL_MAP;// tamanho LEVEL_MAP [MAP_W] [MAP_H]




    void map_init();

    void map_reinit();

    //Desenha os tiles
    void tiles_draw();

    void map_update();

    void react_collision(TILE* t);

    void map_deinit();




#endif
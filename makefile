# flags de compilaca0
CFLAGS = -Wall 
# bibliotecas a ligar
LDLIBS = `pkg-config --libs= allegro-5 allegro_font-5 allegro_primitives-5 allegro_audio-5 allegro_acodec-5 allegro_image-5`

OBJS = display.o keyboard.o map_handler.o sprites.o common.o hud.o mario.o audio.o playable_character.o game_menu.o npc.o score_handler.o

all: mario

# Regra de ligacao
mario: $(OBJS)
	gcc $(OBJS) -o mario $(CFLAGS) $(LDLIBS)

mario.o: mario.c

common.o: common.c common.h

display.o: display.c display.h common.h

keyboard.o: keyboard.c keyboard.h

map_handler.o: map_handler.c map_handler.h common.h sprites.h keyboard.h

sprites.o: sprites.c sprites.h common.h

hud.o: hud.c hud.h sprites.h display.h

audio.o: audio.c audio.h

playable_character.o: playable_character.c playable_character.h

npc.o: npc.c npc.h

game_menu.o: game_menu.c game_menu.h

score_handler.o: score_handler.c score_handler.h

clean:
	-rm -f *~ *.o

purge: clean
	-rm -f mario
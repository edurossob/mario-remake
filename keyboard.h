// --- keyboard ---
#ifndef __GAME_KEYBOARD__
#define __GAME_KEYBOARD__

#include <allegro5/allegro_primitives.h>
#include "common.h"

#define KEY_SEEN     1
#define KEY_RELEASED 2
unsigned char key[ALLEGRO_KEY_MAX];


void keyboard_init();

void keyboard_set();

void keyboard_update(ALLEGRO_EVENT* event);

#endif
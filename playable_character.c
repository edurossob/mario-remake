#include "playable_character.h"


int l_collision;
int r_collision;
int t_collision;
int b_collision;
int y_adjust;

#define WALKING_ANIMATION_DURATION 6
int walking_animation;

int animation_y; // usado para animação pós perda de vida do pc



void pc_init()
{
    pc.x = 30;
    pc.y = -100;
    pc.lives = 3;
    pc.velocity_y = 0;
    pc.velocity_x = 0;
    pc.invencible_timer = 0;

    walking_animation = 0;
    animation_y = 0;
}

void pc_reinit()
{
    pc.x = 30;
    pc.y = -100;
    pc.velocity_y = 0;
    pc.velocity_x = 0;
}

void pc_revive()
{
    pc.lives = 3;
}

void collide_npc()
{
    int ax1, ay1, ax2, ay2; // Valores do pc
    int bx1, by1, bx2, by2; // Valores do npc

    ax1 = pc.x;
    ay1 = pc.y;
    ax2 = pc.x + MARIO_W;
    ay2 = pc.y + MARIO_H;

    for(int i = 0; i < num_npc; i++)
    {
        if(npcs[i].lives == 0)
            continue;

        bx1 = npcs[i].x;
        by1 = npcs[i].y;
        bx2 = npcs[i].x + ROOMBA_W;
        by2 = npcs[i].y + ROOMBA_H;

        if((ax1 > bx1 && ax1+4 < bx2) || (ax2-4 > bx1 && ax2 < bx2))
            if(ay2 >= by1 && ay2 <= (by1+by2)/2)
            {
                b_collision = 1;
                pc.velocity_y = -4;
                npcs[i].lives = 0;
                increase_score(200);
                return;
            }

        if((ay1 > by1 && ay1 < by2) || (ay2 > by1 && ay2 <= by2)) // Objetos alinhados na linha
            if((ax1 < bx2 && ax1 > bx1) || (ax2 > bx1 && ax2 < bx2))
                got_hit = true;

         
    }

}

bool collide()
{
    int ax1, ay1, ax2, ay2; // Valores do pc
    int bx1, by1, bx2, by2; // Valores do tile

    ax1 = pc.x;
    ay1 = pc.y;
    ax2 = pc.x + MARIO_W;
    ay2 = pc.y + MARIO_H;

    l_collision = 0;
    r_collision = 0;
    t_collision = 0;
    b_collision = 0;


    for(int i = 0; i < MAP_H; i++)
        for(int j = 0; j < MAP_W; j++)
            if(LEVEL_MAP[i][j].type > 0)
            {   
                bx1 = LEVEL_MAP[i][j].x;
                by1 = LEVEL_MAP[i][j].y;
                bx2 = LEVEL_MAP[i][j].x + TILE_W;
                by2 = LEVEL_MAP[i][j].y + TILE_H;
                //printf("[%d.%d] (%d, %d) (%d, %d)\n", i, j, ax1, ax2, bx1, bx2);

                if((ax1 > bx1 && ax1+3 < bx2) || (ax2-3 > bx1 && ax2 < bx2)) // Lado esquerdo ou direito do pc alinhado com o bloco
                {
                    if(ay2 >= by1 && ay2 <= by2)
                        b_collision = 1;

                    if(ay1 < by2-1 && ay1 >= by1){
                        t_collision = 1;
                        react_collision(&LEVEL_MAP[i][j]);
                    }


                    if(ay2 + pc.velocity_y > by1 && ay2 + pc.velocity_y <= by2) // Verifica se, com a gravidade, o pc entrará no bloco no proximo mov
                        pc.velocity_y = by1 - ay2; // E ajusta a trajetória para fora do bloco caso necessário
                    
                }

                if((ay1+1 > by1 && ay1+1 < by2) || (ay2 > by1 && ay2 <= by2)) // Objetos alinhados na linha
                {
                    if(ax1 < bx2 && ax1 > bx1)
                        l_collision = 1;
                    if(ax2 > bx1 && ax2 < bx2)
                        r_collision = 1;


                    if(ax1-VISION_DIFF < 0)
                        pc.velocity_x = 1;
                    if(ax2-VISION_DIFF > BUFFER_W)
                        pc.velocity_x = -1;


                    if(pc.velocity_x > 0)
                        if(ax2 + pc.velocity_x > bx1 && ax2 + pc.velocity_x < bx2) // Evita entrar no bloco pela esquerda
                            pc.velocity_x = bx1 - ax2;

                    if(pc.velocity_x < 0)
                        if(ax1 + pc.velocity_x < bx2 && ax1 + pc.velocity_x > bx1) // Evita entrar no bloco pela direita
                            pc.velocity_x = bx2 - ax1-1;
                    

                }


            }

    if(t_collision || b_collision || l_collision || r_collision)
        return true;
    return false;
}

void pc_update()
{
    if(frames < 400) // Apenas para entrada triunfal com a música
        return;

    if(is_game_ended)
        return;

    if(is_paused)
        return;

    got_hit = false;

    // -- Calcula desaceleração
    if(pc.velocity_x > 0.2)
        pc.velocity_x -= 0.1;
    else
        if(pc.velocity_x < -0.2)
            pc.velocity_x += 0.1;
        else
            pc.velocity_x = 0;


    // -- Calcula movimento no eixo x
    if(key[ALLEGRO_KEY_LEFT])
        pc.velocity_x -= PC_SPEED;

    if(key[ALLEGRO_KEY_RIGHT])
        pc.velocity_x += PC_SPEED;


    
    if(pc.velocity_y < PC_MAX_VELOCITY_Y)
        pc.velocity_y += GRAVITY;


    // Calcula colisoes com o mapa e com os npcs
    collide();
    
    collide_npc();
    


    if(b_collision)
    {
        if(pc.velocity_y > 0)
            pc.velocity_y = 0;
        

        if(key[ALLEGRO_KEY_UP]){
            if(pc.velocity_y >= 0)
                pc.velocity_y -= JUMP_SPEED;
        }
    }

    if(t_collision)
        if(pc.velocity_y < 0)
            pc.velocity_y = 0;

    if(l_collision)
        if(pc.velocity_x < 0)
            pc.velocity_x = 0;

    if(r_collision)
        if(pc.velocity_x > 0)
            pc.velocity_x = 0;


    if(pc.velocity_x < PC_MAX_VELOCITY_X*(-1))  // Veirificador de velocidade máxima esquerda
        pc.velocity_x = -PC_MAX_VELOCITY_X;
    if(pc.velocity_x > PC_MAX_VELOCITY_X)       // Veirificador de velocidade máxima direita
        pc.velocity_x = PC_MAX_VELOCITY_X;



    pc.y += pc.velocity_y;
    pc.x += pc.velocity_x;

    if((pc.y + MARIO_H > BUFFER_H))
        got_hit = true;; 

}

void pc_draw()
{

    if(is_game_ended || got_hit)
        return;

    if(pc.velocity_y != 0)
    {

        if(pc.velocity_x >= 0)
            al_draw_bitmap(sprites.mario[PC_SPRITE_SM_JUMP_R], pc.x-VISION_DIFF, pc.y, 0);
        if(pc.velocity_x < 0)
            al_draw_bitmap(sprites.mario[PC_SPRITE_SM_JUMP_L], pc.x-VISION_DIFF, pc.y, 0);
        
        return;
    }

    walking_animation++;



    if(walking_animation > WALKING_ANIMATION_DURATION + 3) // Mario ficará 3 frames na animação de parado qnd estiver correndo
        walking_animation = 0;

        
    if(pc.velocity_x > 0)
    {
        if(walking_animation < WALKING_ANIMATION_DURATION)
        {
            al_draw_bitmap(sprites.mario[PC_SPRITE_SM_WALK_R], pc.x-VISION_DIFF, pc.y, 0);
            return;
        }
        
        al_draw_bitmap(sprites.mario[PC_SPRITE_SM_R], pc.x-VISION_DIFF, pc.y, 0);


        return;
    }
    if(pc.velocity_x < 0)
    {
        if(walking_animation < WALKING_ANIMATION_DURATION)
        {
            al_draw_bitmap(sprites.mario[PC_SPRITE_SM_L], pc.x-VISION_DIFF, pc.y, 0);
            return;
        }

        al_draw_bitmap(sprites.mario[PC_SPRITE_SM_WALK_L], pc.x-VISION_DIFF, pc.y, 0);
        return;
    }
    

    al_draw_bitmap(sprites.mario[PC_SPRITE_SM_R], pc.x-VISION_DIFF, pc.y, 0);

}

int pc_death_animation_draw()
{


    if(frames < 120) // fica 2 segundos no mesmo lugar
        animation_y = pc.y;

    if(frames >= 120 && frames <  160) // fica 1 segundos subindo 
        animation_y -= 3;

    if(frames >=  160 && frames < 280) // fica 2 segundos caindo
        animation_y += 6;

    
    al_draw_bitmap(sprites.mario[PC_SPRITE_SM_DEAD], pc.x - VISION_DIFF, animation_y, 0);

    if(frames >= 280)
        return true;

    return false;
}


void pc_deinit()
{
    return;
}

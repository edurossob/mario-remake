#ifndef __MUST_INIT__
#define __MUST_INIT__

    #include <stdio.h>
    #include <stdlib.h>
    #include <stdbool.h>


    #define MAX_TIME 230 // Tempo máximo do jogo em segundos

    int really_done; // Finaliza completamente o jogo 

    long frames;
    long game_score;
    int coin;
    long seconds_played; // Contador em segundos

    bool is_paused;
    bool is_game_ended;

    // Teste para inicializações
    void must_init(bool test, const char *description);

    void coin_add();


    void increase_score(int score_increase);


#endif
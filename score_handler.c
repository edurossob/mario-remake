#include "score_handler.h"

int cmpInt (const void * a, const void * b) {
   return ( *(int*)b - *(int*)a);
}

void score_init()
{
    int current_read;
    current_read = 0;

    score_file = fopen("resources/score/score.txt", "r");
    must_init(score_file, "score file");

    // Inicializa todos os scores em 0
    for(int i = 0; i < MAX_N_SCORES; i++)
        best_scores[i] = 0;

    // Lẽ o arquivo inserindo os valores em best_scores

    fscanf(score_file, "%d", &current_read);

    for(int i = 0; i < MAX_N_SCORES; i++)
    {
        if(feof(score_file))
            break;

        best_scores[i] = current_read;
        fscanf(score_file, "%d", &current_read);
    }

    qsort(&best_scores, MAX_N_SCORES, sizeof(int), cmpInt);

}

void score_deinit()
{
    if(game_score > best_scores[MAX_N_SCORES-1])
        best_scores[MAX_N_SCORES-1] = game_score;

    qsort(&best_scores, MAX_N_SCORES, sizeof(int), cmpInt);


    freopen("resources/score/score.txt", "w", score_file);


    for(int i = 0; i < MAX_N_SCORES; i++)
        fprintf(score_file, "%d\n", best_scores[i]);


    fclose(score_file);

}
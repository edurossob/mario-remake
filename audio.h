#ifndef __AUDIO__
#define __AUDIO__

#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>

#include "common.h"

ALLEGRO_AUDIO_STREAM* home_menu_music;
ALLEGRO_AUDIO_STREAM* main_music;
ALLEGRO_SAMPLE* sample_select;
ALLEGRO_SAMPLE* sample_explode[2];

bool is_muted; // Indica se foi mutado pelo teclado

void audio_init();

void audio_reinit(ALLEGRO_AUDIO_STREAM* audio);

void mute_unmute_music();

void play_select_sound();

void play_coin_sound();

void audio_deinit();

#endif
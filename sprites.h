#ifndef __GAME_TILES__
#define __GAME_TILES__

    #include <allegro5/allegro_primitives.h>
    #include <allegro5/allegro_image.h>

    #include "common.h"
    #include "map_handler.h"
    #include "playable_character.h"
    #include "npc.h"

    typedef struct SPRITES
    {
        ALLEGRO_BITMAP* _sheet;
        ALLEGRO_BITMAP* tile[7]; // 7 se refere a TILE_TYPE_N

        ALLEGRO_BITMAP* roomba[3];

        ALLEGRO_BITMAP* mario[7]; // 7 se refere a PC_SPRITE_N

        ALLEGRO_BITMAP* mario_logo;
    } SPRITES;

    SPRITES sprites;


    // Devolve um sub-bitmap do bitmap sprites, de tamanho (x,y) até (x+w, y+h)
    ALLEGRO_BITMAP* sprite_grab(int x, int y, int w, int h);

    // Inicia image_addon e os sprites do jogo
    void sprites_init();

    // Destrói os sprites utilizados no jogo
    void sprites_deinit();


#endif
#include "npc.h"


int l_collision;
int r_collision;
int b_collision;
int y_adjust;

void npc_init()
{
    NPC* temp;

    num_npc = 0;

    max_num_npc = 8; // Inicia com o valor arbitrário de 10 npcs

    temp = malloc(max_num_npc * sizeof(NPC));


    npcs = temp;
}

void npc_realocate()
{
    NPC* temp;
    max_num_npc <<= 1;
    temp = realloc(npcs, max_num_npc * sizeof(NPC));
    npcs = temp;
}


void npc_add(int npc_type, int x, int y)
{
    if(num_npc >= max_num_npc)
        npc_realocate();
    

    npcs[num_npc].x = x-5;
    npcs[num_npc].y = y;
    npcs[num_npc].velocity_y = 0;
    npcs[num_npc].velocity_x = 1;//-1
    npcs[num_npc].npc_type = npc_type;


    num_npc ++;

}

void npc_reinit()
{
    num_npc = 0;
}


bool npc_collide(NPC a)
{
    int ax1, ay1, ax2, ay2; // Valores do npc
    int bx1, by1, bx2, by2; // Valores do tile


    ax1 = a.x;
    ay1 = a.y;
    ax2 = a.x + ROOMBA_W;
    ay2 = a.y + ROOMBA_H;


    l_collision = 0;
    r_collision = 0;
    b_collision = 0;


    for(int i = 0; i < MAP_H; i++)
        for(int j = 0; j < MAP_W; j++)
            if(LEVEL_MAP[i][j].type > 0)
            {   
                bx1 = LEVEL_MAP[i][j].x;
                by1 = LEVEL_MAP[i][j].y;
                bx2 = LEVEL_MAP[i][j].x + TILE_W;
                by2 = LEVEL_MAP[i][j].y + TILE_H;
                //printf("[%d.%d] (%d, %d) (%d, %d)\n", i, j, ax1, ax2, bx1, bx2);

                if((ax1 > bx1 && ax1+3 < bx2) || (ax2-3 > bx1 && ax2 < bx2)) // Lado esquerdo ou direito do npc alinhado com o bloco
                {
                    if(ay2 > by1-1 && ay2 <= by2)
                        b_collision = 1;

                    if(ay2 + a.velocity_y > by1 && ay2 + a.velocity_y <= by2) // Verifica se, com a gravidade, o npc entrará no bloco no proximo mov
                        y_adjust = (by1-ay2); // E ajusta a trajetória para fora do bloco caso necessário
                    
                }

                if(( (ay1+ay2)/2 > by1 && (ay1+ay2)/2 < by2 )) // Objetos alinhados na linha
                {
                    if(ax1+1 < bx2 && ax1 > bx1)
                        l_collision = 1;
                    if(ax2-1 > bx1 && ax2 < bx2)
                        r_collision = 1;
                }
                

                if(ax2 >= (MAP_W * TILE_W))
                    r_collision = 1;

            }

    if(b_collision || l_collision || r_collision)
        return true;
    return false;

}


void npc_update()
{
    if(is_paused)
        return;

    if(frames < 435)
        return;


    for(int i = 0; i < num_npc; i++)
    {
        if(npcs[i].lives == 0)
            continue;


        if(npcs[i].velocity_y < NPC_MAX_VELOCITY_Y)
            npcs[i].velocity_y += GRAVITY;


        l_collision = 0;
        r_collision = 0;
        b_collision = 0;
        y_adjust = 0;

        npc_collide(npcs[i]);

        npcs[i].y += y_adjust;

        if(b_collision || y_adjust < 0)
            npcs[i].velocity_y = 0;

        if(l_collision || r_collision)
            npcs[i].velocity_x *= -1;


        npcs[i].y += npcs[i].velocity_y;
        npcs[i].x += npcs[i].velocity_x;

    }
}


void npc_draw()
{
    if(frames < 435)
        return;

    for (int i = 0; i < num_npc; i++)
    {
        if(npcs[i].lives == 0)
            continue;
        al_draw_bitmap(sprites.roomba[1], npcs[i].x-VISION_DIFF, npcs[i].y, 0);
    }

}

void npc_deinit()
{
    free(npcs);
}
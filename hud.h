#ifndef __GAME_HUD__
#define __GAME_HUD__

    #include <allegro5/allegro_font.h>

    #include "sprites.h"
    #include "display.h"
    #include "audio.h"
    #include "common.h"

    ALLEGRO_FONT* font;
    long score_display;

    void hud_init();

    void hud_deinit();

    void hud_update();

    void hud_draw();



#endif
#include "map_handler.h"

#define CAMERA_MAX_SPEED 2



int camera_velocity;


// Retorna a linha e insere em len o tamanho dela
int read_line(char *line, size_t* len){
    char character;
    int tam;

    strcpy(line, "");
    tam = 0;

    character = fgetc(map_file);
    while(character != '\n'){

        if(character == EOF)
            return EOF;
        strncat(line, &character, sizeof(char));
        character = fgetc(map_file);

        tam++;
    }

    *len = tam;
    return 1;
}

// Lê os metadados do arquivo .mapa
void read_meta()
{
    char* line;
    size_t len;
    len = 1024;

    line = malloc(len * sizeof(char));
    
    // Lê a primeira linha, referente ao nome do mapa
    if(read_line(line, &len) != EOF)
        strncpy(MAP_NAME, line, len);

    // Lê a segunda linha, referente à largura e altura do mapa respectivamente
    if(read_line(line, &len) != EOF)
        sscanf(line, "%d %d", &MAP_W, &MAP_H);

    free(line);
}

void map_tiles_init()
{
    for(int i = 0; i < MAP_H; i++)
        for(int j = 0; j < MAP_W; j++){
            LEVEL_MAP[i][j].x = j * TILE_W;
            LEVEL_MAP[i][j].y = i * TILE_H;
            LEVEL_MAP[i][j].type = 0;
            LEVEL_MAP[i][j].is_broken = false;
            LEVEL_MAP[i][j].prize = PRIZE_NONE;
        }

}

void read_map()
{
    int num_temp;

    for(int i = 0; i < MAP_H; i++)
        for(int j = 0; j < MAP_W; j++){
            fscanf(map_file, " %d", &num_temp);

            if(num_temp >= 50)
                npc_add(num_temp-50, j * TILE_W, i * TILE_H);
            else
            {
                LEVEL_MAP[i][j].type = num_temp;

                if(LEVEL_MAP[i][j].type == TILE_TYPE_QUESTION_ON)
                {
                    fscanf(map_file, " %d", &num_temp);
                    LEVEL_MAP[i][j].prize = num_temp;
                }
            }   
        }
}

void allocate_map()
{
    // Matriz temporaria cujo endereço será utilizado na matriz LEVEL_MAP
    TILE** temp;

    // Aloca um vetor de MAP_H ponteiros para as colunas
    temp = malloc(MAP_H * sizeof(TILE*));

    // Aloca um espaço com o tamanho necessário para comportar todos os tiles do mapa
    temp[0] = malloc(MAP_H * MAP_W * sizeof(TILE));

    // Aponta cada ponteiro para o local adequado do passo anterior
    for(int i = 1; i < MAP_H; i++)
        temp[i] = temp[0] + i * MAP_W;

    LEVEL_MAP = temp;
}

void map_init()
{
    map_file = fopen("resources/mapas/1-1.mapa", "r");
    must_init(map_file, "arquivo de mapa 1-1.mapa");

    // Lê o nome e as dimensões do mapa
    read_meta();
    //printf("O mapa '%s', de tamanho %dx%d vai ser brabo\n", MAP_NAME, MAP_W, MAP_H);

    // Com base nas dimensões do mapa, aloca a quantidade máxima necessária de tiles em LEVEL_MAP
    allocate_map();

    map_tiles_init();

    read_map();

    VISION_DIFF = 0;
}

void map_reinit()
{
    fseek(map_file, 0, SEEK_SET);
    read_meta();
    map_tiles_init();
    read_map();
    VISION_DIFF = 0;
}


void tiles_draw()
{
    for(int i = 0; i < MAP_H; i++)
        for(int j = 0; j < MAP_W; j++){
            if(LEVEL_MAP[i][j].type == 0)
                continue;

            if(LEVEL_MAP[i][j].is_broken)
                continue;
            if(LEVEL_MAP[i][j].type >= TILE_TYPE_N)
                continue;

            al_draw_bitmap(sprites.tile[LEVEL_MAP[i][j].type], LEVEL_MAP[i][j].x - VISION_DIFF, LEVEL_MAP[i][j].y, 0);
        }
}

void react_collision(TILE* t)
{
    switch(t->type)
    {

    case TILE_TYPE_BRICK_TOP: t->type = 0;
        break;
    case TILE_TYPE_BRICK: t->type = 0;
        break;
    case TILE_TYPE_QUESTION_ON:
        t->type = TILE_TYPE_QUESTION_OFF;
        play_coin_sound();
        if(t->prize == PRIZE_COIN){
            increase_score(100);
            coin_add();
        }
        break;
    case TILE_TYPE_GROUND:
        break;
    case TILE_TYPE_QUESTION_OFF:
        break;
    case TILE_TYPE_SQUARE:
        break;
    case TILE_TYPE_N:
        break;
    }

}


void map_update()
{
    if(is_game_ended)
        return;

    if(is_paused)
        return;

    if(got_hit)
        return;



    if(pc.x - VISION_DIFF >= CAMERA_CENTER_X + BUFFER_W /16)
        if(camera_velocity <= CAMERA_MAX_SPEED)
            camera_velocity += 1;
        
    

    if(pc.x - VISION_DIFF < CAMERA_CENTER_X + 3)
        camera_velocity = 0 ;



    // Mantém a camera dentro do mapa caso ela saia

    if(VISION_DIFF + BUFFER_W > (MAP_W * TILE_W) - 2) // Borda direita
        camera_velocity = 0;

    if(VISION_DIFF < 0 ) // Borda esquerda
        camera_velocity = 0;


    VISION_DIFF += camera_velocity;

}


void map_deinit()
{
    fclose(map_file);

    free(LEVEL_MAP[0]);
    //free(LEVEL_MAP);
}


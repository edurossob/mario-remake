#ifndef __GAME_DISPLAY__
#define __GAME_DISPLAY__

    #include <allegro5/allegro_primitives.h>
    #include "common.h"


    #define BUFFER_W 320
    #define BUFFER_H 240

    #define DISP_SCALE 2

    #define DISP_W (BUFFER_W * DISP_SCALE)
    #define DISP_H (BUFFER_H * DISP_SCALE)


    ALLEGRO_DISPLAY* disp;
    ALLEGRO_BITMAP* buffer;


    void disp_init();

    void disp_deinit();

    void disp_pre_draw();

    void disp_post_draw();


#endif
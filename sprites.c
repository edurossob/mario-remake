#include "sprites.h"


ALLEGRO_BITMAP* sprite_grab(int x, int y, int w, int h)
{
    ALLEGRO_BITMAP* sprite = al_create_sub_bitmap(sprites._sheet, x, y, w, h);
    must_init(sprite, "sprite grab");
    return sprite;
}

void sprites_init()
{
    must_init(al_init_image_addon(), "image addon");

    // Inicia o próprio spritesheet em um bitmap
    sprites._sheet = al_load_bitmap("resources/sprites/spritesheet.png");
    must_init(sprites._sheet, "spritesheet.png");

    // Pega sub-bitmaps para cada sprite necessário do spritesheet
    sprites.tile[TILE_TYPE_GROUND] = sprite_grab(0, 0, TILE_W, TILE_H);
    sprites.tile[TILE_TYPE_BRICK_TOP] = sprite_grab(17, 0, TILE_W, TILE_H);    
    sprites.tile[TILE_TYPE_BRICK] = sprite_grab(33, 0, TILE_W, TILE_H);
    sprites.tile[TILE_TYPE_QUESTION_ON] = sprite_grab(49, 0, TILE_W, TILE_H);
    sprites.tile[TILE_TYPE_QUESTION_OFF] = sprite_grab(65, 0, TILE_W, TILE_H);
    sprites.tile[TILE_TYPE_SQUARE] = sprite_grab(81, 0, TILE_W, TILE_H);

    sprites.roomba[0] = sprite_grab(128, 0, ROOMBA_W, ROOMBA_H);
    sprites.roomba[1] = sprite_grab(146, 0, ROOMBA_W, ROOMBA_H);
    sprites.roomba[2] = sprite_grab(164, 0, ROOMBA_W, ROOMBA_H);

    sprites.mario[PC_SPRITE_SM_R] = sprite_grab(182, 0, MARIO_W-3, MARIO_H);
    sprites.mario[PC_SPRITE_SM_L] = sprite_grab(199, 0, MARIO_W-3, MARIO_H);
    sprites.mario[PC_SPRITE_SM_WALK_R] = sprite_grab(212, 0, MARIO_W, MARIO_H);
    sprites.mario[PC_SPRITE_SM_WALK_L] = sprite_grab(230, 0, MARIO_W, MARIO_H);
    sprites.mario[PC_SPRITE_SM_JUMP_R] = sprite_grab(245, 0, MARIO_W, MARIO_H);
    sprites.mario[PC_SPRITE_SM_JUMP_L] = sprite_grab(262, 0, MARIO_W, MARIO_H);
    sprites.mario[PC_SPRITE_SM_DEAD] = sprite_grab(278, 0, MARIO_W, MARIO_H);

    sprites.mario_logo = sprite_grab(0, 95, 220, 97);
}


void sprites_deinit()
{

    for(int i = 0; i < TILE_TYPE_N-1; i++)
        al_destroy_bitmap(sprites.tile[i]);

    al_destroy_bitmap(sprites._sheet);

    for(int i = 0; i< PC_SPRITE_N-1; i++)
        al_destroy_bitmap(sprites.mario[i]);

    al_destroy_bitmap(sprites.mario_logo);
}
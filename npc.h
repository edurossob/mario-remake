#ifndef __NPC__
#define __NPC__

    #include <stdlib.h>
    #include "sprites.h"
    #include "map_handler.h"

    typedef struct NPC
    {
        int x, y;
        float velocity_y; // Velocidade para queda e pulos
        float velocity_x;
        int lives;
        int npc_type;
    } NPC;

    NPC* npcs;

    #define ROOMBA_H 16
    #define ROOMBA_W 15

    #define NPC_MAX_VELOCITY_Y 5

    int num_npc;
    int max_num_npc;

    void npc_init();

    void npc_reinit();

    void npc_update();

    void npc_draw();

    void npc_add(int npc_type, int x, int y);

    void npc_deinit();


#endif
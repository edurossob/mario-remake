#include "audio.h"

void audio_init()
{
    al_install_audio();
    al_init_acodec_addon();
    al_reserve_samples(128);

    
    home_menu_music = al_load_audio_stream("resources/sounds/OverworldTheme.opus", 2, 2048);
    must_init(home_menu_music, "home menu music");
    al_set_audio_stream_playmode(home_menu_music, ALLEGRO_PLAYMODE_LOOP);


    main_music = al_load_audio_stream("resources/sounds/At doom's gate.opus", 2, 2048);
    must_init(main_music, "main music");
    al_set_audio_stream_playmode(main_music, ALLEGRO_PLAYMODE_LOOP);


    
    is_muted = false;

    sample_select = al_load_sample("resources/sounds/select.wav");
    must_init(sample_select, "select sample");

}

void audio_reinit(ALLEGRO_AUDIO_STREAM* audio){
    al_rewind_audio_stream(audio);
    al_attach_audio_stream_to_mixer(audio, al_get_default_mixer());
}



void mute_unmute_music()
{

    if(is_game_ended)
    {
        al_set_audio_stream_playing(main_music, false);
        return;
    }

    al_set_audio_stream_playing(main_music, (!is_paused && !is_muted));


}

void play_select_sound()
{
    al_play_sample(sample_select, 0.75, 0, 1, ALLEGRO_PLAYMODE_ONCE, NULL);
}

void play_coin_sound()
{
    al_play_sample(sample_select, 0.75, 0, 1, ALLEGRO_PLAYMODE_ONCE, NULL);
}


void audio_deinit()
{
    al_destroy_audio_stream(home_menu_music);
    al_destroy_audio_stream(main_music);
    al_destroy_sample(sample_select);
    /*
    al_destroy_sample(sample_explode[0]);
    al_destroy_sample(sample_explode[1]);
*/
}
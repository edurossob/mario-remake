#include "common.h"



void must_init(bool test, const char *description)
{
    if(test) return;

    printf("impossivel inicializar %s\n", description);
    
    really_done = true;
    exit(1);
}

void coin_add()
{
    coin++;
}

void increase_score(int score_increase)
{
    game_score += score_increase;
}
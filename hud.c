#include "hud.h"

void hud_init()
{
    font = al_create_builtin_font();
    must_init(font, "font");

    score_display = 0;
}

void hud_deinit()
{
    al_destroy_font(font);
}

void hud_update()
{
    if(frames % 2)
        return;

    for(long i = 5; i > 0; i--)
    {
        long diff = 1 << i;
        if(score_display <= (game_score - diff))
            score_display += diff;
    }
}

void hud_draw()
{
    int font_height;

    font_height = al_get_font_line_height(font);

    // -- SCORE
    al_draw_text(
        font, al_map_rgb_f(1,1,1),
        BUFFER_W/8, 2, ALLEGRO_ALIGN_CENTER,
        "SCORE"
    );

    al_draw_textf(
        font, al_map_rgb_f(1,1,1),
        BUFFER_W/8, font_height + 4, ALLEGRO_ALIGN_CENTER,
        "%ld", score_display
    );

    // -- COINS
    al_draw_text(
        font, al_map_rgb_f(1,1,1),
        (BUFFER_W/3)-10, 2, ALLEGRO_ALIGN_CENTER,
        "COINS"
    );

    al_draw_textf(
        font, al_map_rgb_f(1,1,1),
        (BUFFER_W/3)-10, font_height + 4, ALLEGRO_ALIGN_CENTER,
        "%d", coin
    );

    // -- WORLD

    al_draw_text(
        font, al_map_rgb_f(1,1,1),
        BUFFER_W/2, 2, ALLEGRO_ALIGN_CENTER,
        "WORLD"
    );

    al_draw_text(
        font, al_map_rgb_f(1,1,1),
        BUFFER_W/2, font_height + 4, ALLEGRO_ALIGN_CENTER,
        MAP_NAME
    );

    // -- TIME
    al_draw_text(
        font, al_map_rgb_f(1,1,1),
        BUFFER_W - (BUFFER_W/3) + 10, 2, ALLEGRO_ALIGN_CENTER,
        "TIME"
    );

    al_draw_textf(
        font, al_map_rgb_f(1,1,1),
        BUFFER_W - (BUFFER_W/3) + 10, font_height + 4, ALLEGRO_ALIGN_CENTER,
        "%ld", (MAX_TIME - seconds_played)
    );
    
    // -- LIVES
    al_draw_text(
        font, al_map_rgb_f(1,1,1),
        BUFFER_W - BUFFER_W/8, 2, ALLEGRO_ALIGN_CENTER,
        "LIVES"
    );

    al_draw_textf(
        font, al_map_rgb_f(1,1,1),
        BUFFER_W - BUFFER_W/8, font_height + 4, ALLEGRO_ALIGN_CENTER,
        "%d", pc.lives
    );


    if(frames < 435) // Apenas para entrada triunfal com a música
        al_draw_text(
            font,
            al_map_rgb_f(0,0,0),
            BUFFER_W / 2, BUFFER_H / 2,
            ALLEGRO_ALIGN_CENTER,
            "M A R I O"
        );


    if(is_muted)
        al_draw_text(
            font,
            al_map_rgb_f(0,0,0),
            15, BUFFER_H - font_height,
            ALLEGRO_ALIGN_LEFT,
            "M U T E D"
        );

}
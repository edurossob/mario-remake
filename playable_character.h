#ifndef __PC__
#define __PC__

    #include <allegro5/allegro_primitives.h>
    #include "keyboard.h"
    #include "sprites.h"
    #include "npc.h"

    typedef struct PC{
        float x, y;
        int lives;
        float velocity_y; // Velocidade para queda e pulos
        float velocity_x;
        int invencible_timer; // Timer pra quando for atingido 
    } PC;
    PC pc;

    typedef enum PC_SPRITES{
        PC_SPRITE_SM_R,
        PC_SPRITE_SM_L,
        PC_SPRITE_SM_WALK_R,
        PC_SPRITE_SM_WALK_L,
        PC_SPRITE_SM_JUMP_R,
        PC_SPRITE_SM_JUMP_L,
        PC_SPRITE_SM_DEAD,
        PC_SPRITE_N
    } PC_SPRITES;


    #define MARIO_W 14
    #define MARIO_H 16

    #define PC_SPEED 1
    #define PC_MAX_VELOCITY_X 2
    #define PC_MAX_VELOCITY_Y 16 //16
    #define GRAVITY 0.5
    #define JUMP_SPEED 8//8

    int got_hit;

    void pc_init();

    void pc_reinit();

    void pc_revive();

    void pc_update();

    void pc_draw();

    // retorna true quando acabou a animação
    int pc_death_animation_draw();

    void pc_deinit();


#endif
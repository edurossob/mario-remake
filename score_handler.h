#ifndef __SCORE_HANDLER__
#define __SCORE_HANDLER__

    #include <stdlib.h>
    #include "common.h"


    FILE* score_file;

    #define MAX_N_SCORES 10

    int best_scores[MAX_N_SCORES]; // Leremos no máximo os 10 melhores scores


    void score_init();

    void score_deinit();

#endif
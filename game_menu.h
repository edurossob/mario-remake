#ifndef __GAME_MENU__
#define __GAME_MENU__

    #include <allegro5/allegro_font.h>
    #include <allegro5/allegro_primitives.h>

    #include "hud.h"
    #include "common.h"
    #include "sprites.h"
    #include "score_handler.h"


    void home_menu_draw();

    void home_menu_fade_out();

    void game_menu_draw();

    void post_menu_draw();


#endif